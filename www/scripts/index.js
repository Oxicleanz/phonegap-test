﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

})();


//Page 1 - jQuery function
$(document).on('pageshow', '#question1', function () {
    $('#radioColor input').on('change', function () {
        selectedVal = $('input[name="color"]:checked', '#radioColor').val();
        $('#result1').text(selectedVal);
    });
});

//Page 2 - jQuery function
$(document).on('pageshow', '#question2', function () {
    $("#checkboxSports input").on("change", function () {
        var sports = [];
        $('input[name=sport]:checked').each(function () {

            var sportTypes = $('label[for=' + this.id + ']').text();
            sports.push(sportTypes);
        });
        $("#result2").html(sports.join(", "));
    });
});

//Page 3 - jQuery function
$(document).on('pageshow', '#question3', function () {
    $("#foodInput")
                     .keyup(function () {
                         var value = $(this).val();
                         $("#result3").text(value);
                     })
                     .keyup();
});

//Page 4 - jQuery function
$(document).on('pageshow', '#question4', function () {
    $("#musicInput")
                    .keyup(function () {
                        var value = $(this).val();
                        $("#result4").text(value);
                    })
                    .keyup();
});

//Page 5 - jQuery function
$(document).on('pageshow', '#question5', function () {
    $('#radioAnimal input').on('change', function () {
        selectedVal = $('input[name="animal"]:checked', '#radioAnimal').val();
        $('#result5').text(selectedVal);
    });
});

//Page 6- jQuery function
$(document).ready(function myFunction() {
    var x = document.getElementById("rating").value;
    document.getElementById("result6").innerHTML = x;
    document.getElementById("submitBtn").onclick = function () {
        location.href = "#results";
        myFunction()
    };
});

//Facebook Stuff
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        testAPI();
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        document.getElementById('status').innerHTML = 'Please log ' +
          'into this app.';
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        document.getElementById('status').innerHTML = 'Please log ' +
          'into Facebook.';
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '373251219536093',
        cookie: true,  // enable cookies to allow the server to access
        // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.2' // use version 2.2
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });

};

// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function (response) {
        console.log('Successful login for: ' + response.name);
        document.getElementById('status').innerHTML =
          'Thanks for logging in, ' + response.name + '!';
    });
}

//Checks the status of the user in order to change the login/log out button      
FB.Event.subscribe('auth.statusChange', function (response) {
    Log.info('Status Change Event', response);
});

